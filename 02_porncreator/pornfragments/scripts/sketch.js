
//global vars

var cardCategories = [];
var cards = [];

var backLines = [];
var lineC;
var colorSelect;
var aLine;

var mOverCard = false;
var mOverTopCardID;
var xDif, yDif;
var dragLock = false;
var isPainting = false;

var debugOn = false;

var stacked = true;
var stackT = 1;
var sStack;

var rowCount = 0;
var rowsAndColumns = [];

var mCheckbox = [];
var mCheckboxRows = [];
var changedCheckboxId;

var backImage;

//momentum vars
var dragMult = 0.9;
var oldMouseX;
var oldMouseY;



// p5 method: setup is called when the website is first loaded
function setup() {
	var myCanvas = createCanvas(windowWidth, windowHeight);
	myCanvas.parent("myCanvas");

	rectMode(CENTER);

	//myFont = loadFont("fonts/HelveticaNeue-Thin.otf");
	textFont("Helvetica Neue");
	//textStyle(0);

	loadCardCategories();
	loadCards();

	for (i=0; i<cardCategories.length; i++){
		if (cardCategories[i].tRow > rowCount){
			rowCount = cardCategories[i].tRow;
		}
	}
	rowCount++;

	// composite the rows & collumns of each stack
	for (var ii=0; ii<rowCount; ii++){
		rowsAndColumns[ii] = [];
		for (var i=0; i<cardCategories.length; i++) {
			if (cardCategories[i].tRow == ii){
				rowsAndColumns[ii].push(i);
			}
		}
	}
	//print(rowsAndColumns);

	shuffleCards();

	createMenu();

	lineC = color(255, 0, 0);
}

// p5 method: draw is called every frame
function draw() {
  	clear();

  	drawBack();

  	if (!dragLock){

		if (mouseOverVisibleCard()){
  			mOverCard = true;
  			mOverTopCardID = getTopCardID();
				cursor(MOVE);
  		} else {
  			mOverCard = false;
				cursor(CROSS);
  		}
  	}

	for (var i=0; i<cards.length; i++) {

  		if (i == mOverTopCardID){
  			cards[i].mOverAndTopCard = true;
  		} else {
  			cards[i].mOverAndTopCard = false;
  		}

        cards[i].update();
  		cards[i].display();
  	}

	if (debugOn){
		noStroke();
		fill(200);
		strokeWeight(0);
		textSize(12);
		text("frameRate: " + nf(frameRate(), 2, 2), 800, 40);
    }

    oldMouseX = mouseX;
    oldMouseY = mouseY;
}


function drawBack(){
	for (var i=0; i<backLines.length; i++) {
		backLines[i].draw();
	}
}

//p5 method: called on mouse pressed
function mousePressed() {

	if (mOverCard){
		dragLock = true;
 		cards[mOverTopCardID].newRotation();

		if (cards[mOverTopCardID].mouseOverLink() && cards[mOverTopCardID].cType == "link"){
  			window.open(cards[mOverTopCardID].cLink,"_blank");
				mouseReleased();
  		}
	} else {
		isPainting = true;
		aLine = backLines.length;
		backLines[aLine] = new backLine(mouseX, mouseY, lineC);
	}
	if (dragLock){
		xDif = mouseX - cards[mOverTopCardID].xPos;
		yDif = mouseY - cards[mOverTopCardID].yPos;
		cards.move(mOverTopCardID, cards.length - 1);
		mOverTopCardID = cards.length - 1;
	}
}

//p5 method: called on mouse dragged
function mouseDragged() {

	if (dragLock){
		cards[mOverTopCardID].xPos = mouseX - xDif;
		cards[mOverTopCardID].yPos = pmouseY - yDif;
	}

	if (isPainting) {
		backLines[aLine].newSegment(mouseX, mouseY);
	}
}

//p5 method: called on mouse released
function mouseReleased() {
	dragLock = false;
    isPainting = false;
    cards[mOverTopCardID].setMomentum(mouseX - oldMouseX, mouseY - oldMouseY);
}

//p5 method: called on keyboard key pressed
function keyPressed() {
	//print(keyCode);

	// d
    if (keyCode === 68) { // if "d" is pressed toggle the debug mode
        if (debugOn) {
            debugOn = false;
        } else {
            debugOn = true;
        }
    }
  	
}

//returns true if the mouse is hovering over a card
function mouseOverVisibleCard() {
	var r = false;
	for (var i=0; i<cards.length; i++) {
		if (cards[i].visible){
			if (cards[i].mouseOver()){
				r = true;
			}
		}
	}
	return r;
}


function getTopCardID() {

	var mOverCIDs = [];
	var mOverCounter = 0;

	for (var i=0; i<cards.length; i++) {
		if (cards[i].mouseOver()){
			if (cards[i].visible){
				mOverCIDs[mOverCounter] = i;
				mOverCounter = mOverCounter + 1;
			}
		}
	}

	var topCard = mOverCIDs[mOverCIDs.length - 1];
	return topCard;
}

function shuffleCards() {

		if (stackT == 0) { // stackType == stack
			for (var i=0; i<cards.length; i++) {
				cards[i].newPos(windowWidth/2, windowHeight/2);
			}
		}

		if (stackT == 1) { // stackType == stacks

			var longestRow = 0;
			for (var iRow=0; iRow<rowsAndColumns.length; iRow++){
				if (rowsAndColumns[iRow].length > rowsAndColumns[longestRow].length){
					longestRow = iRow;
				}
			}
			//print(longestRow);

			// calculate spaces inbetween cards (depending on longestRow)
			var longestRowLength = rowsAndColumns[longestRow].length;
			var border = cards[0].cWidth / 2;
			var cardSpaceWidth = windowWidth - (2 * border);
			var maxDistanceBetweenCards = (cardSpaceWidth - cards[0].cWidth) / (longestRowLength - 1);
			var distanceBetweenCards = maxDistanceBetweenCards;
			if (maxDistanceBetweenCards > cards[0].cWidth * 1.2){
				distanceBetweenCards = cards[0].cWidth * 1.2;
			}

			var rowCount = rowsAndColumns.length;
			var startY = (windowHeight/2) - (distanceBetweenCards * (rowCount-1) / 2);

			for (var iRow=0; iRow<rowsAndColumns.length; iRow++){

				var columnLength = rowsAndColumns[iRow].length;
				var startX = (windowWidth/2) - (distanceBetweenCards * (columnLength-1) / 2);

				for (var iColumn=0; iColumn<columnLength; iColumn++){

					for (var iCards=0; iCards<cards.length; iCards++) {

						if (cards[iCards].cID == rowsAndColumns[iRow][iColumn]){

							cards[iCards].newPos(startX + (iColumn * distanceBetweenCards), startY + (iRow * distanceBetweenCards));
						}
					}
				}
			}
		}

		if (stackT == 2) { // stackType == chaos
			var b = 50;
			for (var i=0; i<cards.length; i++) {
				cards[i].newPos(random(b, windowWidth - b), random(b, windowHeight - b));
			}
		}

		for (var i=0; i<cards.length; i++) {
			cards[i].newRotation();
		}

	cards.shuffle();
	if (stackT == 1){
		separateStacks();
	} else {
		//moveFirstRowCardsToTop();
	}
}

function separateStacks(){
	for (var i=cardCategories.length; i>=0; i--){
		for (var j=0; j<cards.length; j++) {
			if (cards[j].cID == i) {
				cards.move(j, cards.length - 1);
			}
		}
	}
}

function moveFirstRowCardsToTop(){
	for (var i=0; i<cards.length; i++) {
		if (cards[i].cID == 0) {
			cards.move(i, cards.length - 1);
		}
	}
}


//adds the Header and Footer to the website
function createMenu() {

	// ===========================================================

	var myTopMenu = createDiv("").parent("myHeader");

	// card types selection

	var eachRow = [];
	for (var i=0; i<rowsAndColumns.length; i++){
		//eachRow.push();
		eachRow[i] = createDiv("").parent("myHeader");

		mCheckboxRows[i] = createCheckbox(">", true).style("display: inline-block;").parent(eachRow[i]);
		mCheckboxRows[i].changed(toggleRows);

		for (var ii=0; ii<rowsAndColumns[i].length; ii++) {

			mCheckbox[rowsAndColumns[i][ii]] = createCheckbox(cardCategories[rowsAndColumns[i][ii]].tTitle, true).style("display: inline-block;").parent(eachRow[i]);
			mCheckbox[rowsAndColumns[i][ii]].changed(toggleType);
		}
	}

	// ===========================================================

	// right side

	var myFooterRight = createDiv("").style("float:right; text-align:right").parent("myFooter");

	var myPDFs = createDiv("").parent(myFooterRight);
	createA("pdfs.html", "PDFs", "_blank").parent(myPDFs);
	//createSpan(" / ").style("cursor:default;").parent(myPDFs);

	var myBitbucket = createDiv("").parent(myFooterRight);
	createA("https://bitbucket.org/csongorb/porncreator", "fork me on Bitbucket", "_blank").parent(myBitbucket);

	// left side

	var myFooterLeft = createDiv("").style("float:left; text-align:left").parent("myFooter");

	// change drawing color

	var myColorChanger = createDiv("").parent(myFooterLeft);

	colorSelect = createSelect().parent(myColorChanger);
	colorSelect.option("r");
	colorSelect.option("g");
	colorSelect.option("b");
	colorSelect.changed(changeColor);

	// stacks & shuffle

	var myStacks = createDiv("").parent(myFooterLeft);

	createSpan(" ").style("cursor:default;").parent(myStacks);

	sStack = createSelect().parent(myStacks);
	sStack.option("stacks");
	sStack.option("stack");
	sStack.option("chaos");
	sStack.changed(changeStack);

	createSpan(" / ").style("cursor:default;").parent(myStacks);

	var sShuffle = createSpan("shuffle").style("cursor:pointer;").parent(myStacks);
	sShuffle.mousePressed(shuffleCards);

}

function toggleRows() {

	for (var i=0; i<mCheckboxRows.length; i++){
		for (var ii=0; ii<rowsAndColumns[i].length; ii++){
			if (mCheckboxRows[i].checked() == true){
				mCheckbox[rowsAndColumns[i][ii]].checked(true);
			} else {
				mCheckbox[rowsAndColumns[i][ii]].checked(false);
			}
		}
	}
	toggleType();
}

function toggleType() {
	for (var i=0; i<cards.length; i++) {
		cards[i].visible = true;
	}
	for (var i=0; i<cards.length; i++) {
		// if the visibality of the card IS NOT matching the state of the checkbox...
		if (cards[i].visible != mCheckbox[cards[i].cID].checked()){
			// toggle visibality
			if (cards[i].visible){
				cards[i].visible = false;
			} else {
				cards[i].visible = true;
			}
		} else {
			var canStayVisible = false;
			for (var i2=0; i2<cards[i].cTags.length; i2++) {
				//print(cards[i].cText + ": " + cards[i].cTags[i2] + ": " + mCheckbox[cards[i].cTags[i2]].checked());
				if (mCheckbox[cards[i].cTags[i2]].checked() == true) {
					canStayVisible = true;
				}
			}
			//print(cards[i].cText + ": " + canStayVisible);
			if (canStayVisible){
				// toggle visibality
				cards[i].visible = true;
			} else {
				cards[i].visible = false;
			}
		}
	}
}

function changeStack() {
	var stackType = sStack.value();
	if (stackType == "stack") {
		stackT = 0;
	}
	if (stackType == "stacks") {
		stackT = 1;
	}
	if (stackType == "chaos") {
		stackT = 2;
	}
	shuffleCards();
}

function changeColor() {
	var colorS = colorSelect.value();
	if (colorS == "r") {
		lineC = color(255, 0, 0);
	}
	if (colorS == "g") {
		lineC = color(0, 255, 0);
	}
	if (colorS == "b") {
		lineC = color(0, 0, 255);
	}
}

// ============================
// https://www.kirupa.com/html5/shuffling_array_js.htm

Array.prototype.shuffle = function() {
    var input = this;

    for (var i = input.length-1; i >=0; i--) {

        var randomIndex = Math.floor(Math.random()*(i+1));
        var itemAtIndex = input[randomIndex];

        input[randomIndex] = input[i];
        input[i] = itemAtIndex;
    }
    return input;
}

// ============================
// https://stackoverflow.com/questions/2440700/reordering-arrays

Array.prototype.move = function (from, to) {
  this.splice(to, 0, this.splice(from, 1)[0]);
};
