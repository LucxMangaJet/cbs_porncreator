
import processing.pdf.*;
import controlP5.*;

import java.io.IOException;
//import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

ControlP5 cp5;

// Project paths
String myProjectPath;
String myProcessingPath;
String myInputFolder;
String myPorncreatorFolder;
String myOutputFolder;
String selectedProjectFolder;
String myDomain;

BufferedReader inputToLoad; 
BufferedReader pornSaveFile;

//cards and categories
XML cardsXml;                //SUGGESTION: why not save these informations in a more readable format like csv?
XML cardCategoriesXml;

Card[] cards;
CardCategory[] cardCategories;

//Writers used to create html and js formats for the websites
PrintWriter cardsJs;          //SUGGESTION: a lot of these variables could be defined elsewere. For example cardsJs is only used by createCardsJs() and could be defined in there.
PrintWriter cardCategoriesJs;
PrintWriter cardHtmls;
PrintWriter PDFhtml;

//QR codes for each card, used in createPDFs
PImage[] qrCodes;   


PGraphicsPDF singleCardsPDF;
PGraphicsPDF printPDF;

PFont menuFont;
PFont titleFont;
PFont textFont;

String message = "booo!";  //Contains the info message displayed at the bottom of the application, used to convey progress to the user

//Declaring informations for creating the pdfs 
int cWidth = 63; // in mm    //SUGGESTION: this entire section is only used by createPDFs, maybe should be moved there
int cHeight = 88; // in mm
int cBorder = 3; // in mm
int dpi = 300;
boolean printQRcodes = true;
int cRows = 3;
int cColumns = 3;
int cCornerPx = 30;
int l = 20; // corner length

int cWidthPx = mmToPx(cWidth, dpi);  //SUGGESTION: Why is mmToPx in createPDFs if it is only used here? (Maybe move it to create PDFs too)
int cHeightPx = mmToPx(cHeight, dpi);
int cBorderPx = mmToPx(cBorder, dpi);


void setup() {

  //load usefull paths
  
  inputToLoad = createReader("porncreator.sav");     //SUGGESTION: When reading a custom file extension like ".sav" one expects it to be in a special format. Since all the .sav are actually just .txt files, changing them to .txt would make it quicker to understand
  try {
    selectedProjectFolder = inputToLoad.readLine();
  } 
  catch (IOException e) {
    e.printStackTrace();
    inputToLoad = null;
  }

  myInputFolder = "/01_input";
  myPorncreatorFolder = "/02_porncreator";
  myOutputFolder = "/03_output";

  myProcessingPath = myPorncreatorFolder + "/porncreator";
  myProjectPath = sketchPath().substring(0, sketchPath().length() - myProcessingPath.length());
  
  // load domain path

  pornSaveFile = createReader(myProjectPath + myInputFolder + selectedProjectFolder + "/porn.sav");
  try {
    myDomain = pornSaveFile.readLine();
  } 
  catch (IOException e) {
    e.printStackTrace();
    myDomain = null;
  }
  
  // setup window
    size(250, 400);
    
  // preparing menu

  //println(PFont.list());
  menuFont = createFont("Helvetica Neue", 12);
  titleFont = createFont("HelveticaNeue-UltraLight", 96); // can only use created fonts for PDF printing!
  textFont = createFont("HelveticaNeue-UltraLight", 78);
  textAlign(CENTER);

  int buttonWidth0 = 180; //SUGGESTION: Rename to buttonWidthBig and buttonWidthSmall?
  int buttonWidth1 = 160;

  cp5 = new ControlP5(this);

  cp5.addButton("buildPorn") 
    .setPosition((width/2) - (buttonWidth0/2), 120)
    .setSize(buttonWidth0, 19)
    ;

  cp5.addButton("reloadXMLs")
    .setPosition((width/2) - (buttonWidth1/2), 150)
    .setSize(buttonWidth1, 19)
    ;
  cp5.addButton("prepareBuild")
    .setPosition((width/2) - (buttonWidth1/2), 170)
    .setSize(buttonWidth1, 19)
    ;
  cp5.addButton("copyP5js")
    .setPosition((width/2) - (buttonWidth1/2), 190)
    .setSize(buttonWidth1, 19)
    ;
  cp5.addButton("createJS")
    .setPosition((width/2) - (buttonWidth1/2), 210)
    .setSize(buttonWidth1, 19)
    ;
  cp5.addButton("createHTML")
    .setPosition((width/2) - (buttonWidth1/2), 230)
    .setSize(buttonWidth1, 19)
    ;
  cp5.addButton("copyTexts")
    .setPosition((width/2) - (buttonWidth1/2), 250)
    .setSize(buttonWidth1, 19)
    ;
  cp5.addButton("createPDFs")
    .setPosition((width/2) - (buttonWidth1/2), 270)
    .setSize(buttonWidth1, 19)
    ;

  reloadXMLs(); //SUGGESTION: This should be before visual declarations of the buttons, seeing it here makes me think that it is part of the visualization
}

void draw() {
  background(0);
  textFont(menuFont);

  text ("loaded:", width/2, 20);
  text (selectedProjectFolder, width/2, 40);
  text ("cards: " + cards.length, width/2, 60);
  text ("cardCategories: " + cardCategories.length, width/2, 80);

  text(message, width/2, 330);
}

// ========================================
// Menu Execution Functions
// ========================================

void buildPorn() {
  reloadXMLs();
  prepareBuild();
  copyP5js();
  createJS();
  createHTML();
  copyTextFiles();
  createPDFs();
  message = "build ready!";
}

void reloadXMLs() {
  cardsXml = loadXML(myProjectPath + myInputFolder + selectedProjectFolder + "/cards/cards.xml");
  cardCategoriesXml = loadXML(myProjectPath + myInputFolder + selectedProjectFolder + "/cards/cardCategories.xml");
  loadCardCategories();
  loadCards();
  message = "XMLs (re)loaded";
}

void prepareBuild() {
  deleteExistingBuild();
  message = "old build deleted";
}

void copyP5js() {
  copyPornFiles();
  message = "files copied";
}

void createJS() {
  createCardsJs();
  createCardCategoriesJs();
  message = "JS-files created";
}

void createHTML() {
  createHtmls();
  message = "HTML-files created";
}

void copyTexts() {
  copyTextFiles();
  message = "texts copied";
}

void createPDFs() {
  downloadQRcodes();
  createA4PDF();
  createSingleCardsPDF();
  message = "PDFs created";
}

// ========================================
// Other...
// ========================================

String removeSpecChars(String str) { //SUGGESTION: This can be optimized a lot by using replace() instead of split() and then join()
  String[] words;
  String r;

  // delete special characters
  words = split(str, " ");
  r = join(words, "");
  words = split(r, ".");
  r = join(words, "");
  words = split(r, "-");
  r = join(words, "");

  // replace special characters
  words = split(r, "ä");
  r = join(words, "a");
  words = split(r, "ü");
  r = join(words, "u");
  words = split(r, "ö");
  r = join(words, "o");

  return r;
}
