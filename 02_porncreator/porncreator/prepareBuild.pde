
void deleteExistingBuild() {
  
  String s = myProjectPath + myOutputFolder + selectedProjectFolder;

  File folder = new File(s);

  if (folder.exists()) {
    deleteDirectoryContent(folder);
    folder.delete();
  }

  folder.mkdir();
}

void deleteDirectoryContent(File dir) {

  File[] children = dir.listFiles();
  
  for (int i = 0; i < children.length; i++) {
    if (children[i].isDirectory()) {
      deleteDirectoryContent(children[i]);
    }
    boolean succes = children[i].delete();
    if (succes) { 
      //println("The file was deleted");
    }
  }
}