
// ========================================
// download QR-Codes
// ========================================

void downloadQRcodes() {

  qrCodes = new PImage[cards.length];
  String linkURL = myDomain + "/";

  for (int i0 = 0; i0 < cards.length; i0++) {
    if (cards[i0].type == "link") {
      String fullURL = linkURL + cards[i0].filename + ".html";

      // two alternatives:
      // https://www.the-qrcode-generator.com/
      qrCodes[i0] = loadImage("http://chart.apis.google.com/chart?chs=230x230&cht=qr&chld=|0&chl=" + fullURL, "png");
      // https://zxing.appspot.com/generator
      // qrCodes[i0] = loadImage("https://zxing.org/w/chart?cht=qr&chs=230x230&chld=L&choe=UTF-8&chl=" + fullURL, "png");
    }
  }
}

// ========================================
// create PDF for professional printing (meinspiel.de)
// ========================================

// singlepage PDF (each card on one page)
// http://www.meinspiel.de/selbstgestaltete-spielkarten-mit-fotos-gestalten-drucken
// 55 cards, 63x88mm, 3mm border, 300dpi
// http://www.meinspiel.de/img/grossauflagen/gestaltungsleitfaden_kleinauflage.pdf

void createSingleCardsPDF() {

  singleCardsPDF = (PGraphicsPDF) createGraphics(cWidthPx + cBorderPx, cHeightPx + cBorderPx, PDF, myProjectPath + myOutputFolder + selectedProjectFolder + "/pdfs/singleCards.pdf");

  // general settings
  color textColor = color(220);
  boolean drawBorder = false;

  beginRecord(singleCardsPDF);

  for (int i = 0; i < cards.length; i++) {

    if (i > 0) {
      singleCardsPDF.nextPage();
    }

    if (cards[i].type.equals("link")) {

      // background color
      String temp_c = "FF" + cardCategories[(cards[i].cardCategoryId)].c;
      color cardColor = color(unhex(temp_c));
      background(cardColor);

      // qr code coloring
      PImage qrCodeInverted = qrCodes[i];
      qrCodeInverted.filter(INVERT);
      PImage qrCodeBG = colorSwitch(qrCodeInverted, color(0), cardColor);
      PImage qrCode = colorSwitch(qrCodeBG, color(255), textColor);

      noFill();
      strokeWeight(1);
      stroke(textColor);
      createLinkCard(singleCardsPDF.width/2, singleCardsPDF.height/2, cards[i].title, qrCode);
    }

    if (cards[i].type.equals("display")) {

      // background color
      background(0);

      // color
      String temp_c = "FF" + cardCategories[(cards[i].cardCategoryId)].c;
      color borderColor = color(unhex(temp_c));

      noFill();
      strokeWeight(25);
      stroke(borderColor);
      rect(singleCardsPDF.width/2, singleCardsPDF.height/2, cWidthPx, cHeightPx, cCornerPx);

      noFill();
      strokeWeight(1);
      fill(textColor);
      String[] tmpText = {cards[i].text};
      createDisplayCard(singleCardsPDF.width/2, singleCardsPDF.height/10*4, tmpText);

      // tags
      ellipseMode(CENTER);
      noStroke();
      int distance = 50;
      int r = 20;
      int tagWidth = distance * (cards[i].tags.length - 1);
      for (int i0=0; i0 < cards[i].tags.length; i0++) {
        String temp_color = "FF" + cardCategories[(cards[i].tags[i0])].c;
        color dotColor = color(unhex(temp_color));
        fill(dotColor);
        ellipse(singleCardsPDF.width/2 - (tagWidth/2) + (i0*distance), singleCardsPDF.height/10*9, r*2, r*2);
      }
    }

    // draw border
    noFill();
    stroke(128);
    if (drawBorder) {
      rectMode(CENTER);
      rect(singleCardsPDF.width/2, singleCardsPDF.height/2, cWidthPx, cHeightPx, cCornerPx);
    }
  }
  endRecord();
}

// ========================================
// create PDF for home printing
// ========================================

// dinA4 page for direct printing
// without colors for printing on colored paper

void createA4PDF() {

  printPDF = (PGraphicsPDF) createGraphics(cWidthPx * cColumns, cHeightPx * cRows, PDF, myProjectPath + myOutputFolder + selectedProjectFolder + "/pdfs/cardsA4.pdf");

  beginRecord(printPDF);

  for (int i = 0; i < cards.length; i = i + (cColumns*cRows)) {

    if (i > 0) {
      printPDF.nextPage();
    }

    for (int c = 0; c < cColumns; c++) {
      for (int r = 0; r < cRows; r++) {

        int cNr = i + (r*cColumns) + c;

        if (cNr < cards.length) {

          fill(0);

          if (cards[cNr].type.equals("link")) {
            createLinkCard((c*cWidthPx) + (cWidthPx/2), (r*cHeightPx) + (cHeightPx/2), cards[cNr].title, qrCodes[cNr]);
          }

          if (cards[cNr].type.equals("display")) {
            String[] tmpText = {cards[cNr].text};
            createDisplayCard((c*cWidthPx) + (cWidthPx/2), (r*cHeightPx) + (cHeightPx/2), tmpText);
          }

          // border
          noFill();
          strokeWeight(1);
          stroke(0);

          // draw mark at every left upper corner
          line(c*cWidthPx, r*cHeightPx, c*cWidthPx + l, r*cHeightPx);
          line(c*cWidthPx, r*cHeightPx, c*cWidthPx - l, r*cHeightPx);
          line(c*cWidthPx, r*cHeightPx, c*cWidthPx, r*cHeightPx + l);
          line(c*cWidthPx, r*cHeightPx, c*cWidthPx, r*cHeightPx - l);

          // draw last column of marks
          if (c == cColumns - 1) {
            line(cColumns*cWidthPx, r*cHeightPx, cColumns*cWidthPx + l, r*cHeightPx);
            line(cColumns*cWidthPx, r*cHeightPx, cColumns*cWidthPx - l, r*cHeightPx);
            line(cColumns*cWidthPx, r*cHeightPx, cColumns*cWidthPx, r*cHeightPx + l);
            line(cColumns*cWidthPx, r*cHeightPx, cColumns*cWidthPx, r*cHeightPx - l);
          }

          // draw last row of marks
          if (r == cRows - 1) {
            line(c*cWidthPx, cRows*cHeightPx, c*cWidthPx + l, cRows*cHeightPx);
            line(c*cWidthPx, cRows*cHeightPx, c*cWidthPx - l, cRows*cHeightPx);
            line(c*cWidthPx, cRows*cHeightPx, c*cWidthPx, cRows*cHeightPx + l);
            line(c*cWidthPx, cRows*cHeightPx, c*cWidthPx, cRows*cHeightPx - l);
          }

          // draw last mark
          line(cColumns*cWidthPx, cRows*cHeightPx, cColumns*cWidthPx + l, cRows*cHeightPx);
          line(cColumns*cWidthPx, cRows*cHeightPx, cColumns*cWidthPx - l, cRows*cHeightPx);
          line(cColumns*cWidthPx, cRows*cHeightPx, cColumns*cWidthPx, cRows*cHeightPx + l);
          line(cColumns*cWidthPx, cRows*cHeightPx, cColumns*cWidthPx, cRows*cHeightPx - l);

          //rect((c*cWidthPx) + (cWidthPx/2), (r*cHeightPx) + (cHeightPx/2), cWidthPx, cHeightPx);
        }
      }
    }
  }
  endRecord();
}

// ==================================================================

void createLinkCard(int xPos, int yPos, String[] t, PImage img) {

  boolean drawUnderscore = true;

  textFont(titleFont);
  textAlign(CENTER);
  rectMode(CENTER);

  int textYpos = yPos - (cHeightPx/4);

  strokeWeight(2);
  int underscoreShift = 8;

  for (int i=0; i < t.length; i++) {

    text(t[i], xPos, textYpos + (i * (textAscent() + textDescent())));
    if (drawUnderscore) {
      line(xPos - textWidth(t[i])/2, textYpos + (i * (textAscent() + textDescent())) + underscoreShift, xPos + textWidth(t[i])/2, textYpos + (i * (textAscent() + textDescent())) + underscoreShift);
    }
  }

  imageMode(CENTER);
  int imgYpos = yPos + cHeightPx/4;
  image(img, xPos, imgYpos);
}

void createDisplayCard(int xPos, int yPos, String[] t) {

  textFont(textFont);
  textAlign(CENTER, CENTER);
  rectMode(CENTER);

  int textYpos = yPos;
  int textBorder = 80; // 300 dpi!

  for (int i=0; i < t.length; i++) {
    text(t[i], xPos, textYpos + (i * (textAscent() + textDescent())), cWidthPx - textBorder, cHeightPx - textBorder);
  }
}


int mmToPx(int mm, int dpi) {
  return (int((dpi * mm) / 25.4));
}

// https://forum.processing.org/one/topic/how-to-change-the-color-of-each-pixel-in-an-image.html
PImage colorSwitch(PImage img, color cIn, color cOut) {
  PImage switched = img.get();
  switched.loadPixels();
  for (int i=0; i<switched.pixels.length; i++) {
    color c = switched.pixels[i];
    if (c == cIn) {
      switched.pixels[i] = cOut;
    }
  }
  switched.updatePixels();
  return switched;
}
