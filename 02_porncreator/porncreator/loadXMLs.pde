
// ========================================
// load Card.xml
// ========================================

void loadCards() {
  XML[] allCards = cardsXml.getChildren("card");

  cards = new Card[allCards.length];

  for (int i = 0; i < allCards.length; i++) {

    cards[i] = new Card();

    String type = "link";
    if (allCards[i].hasAttribute("type")) {
      type = allCards[i].getString("type");
    }

    cards[i].type = type;

    // get categoryId
    XML temp_cardCategoryId = allCards[i].getChild("cardCategoryId");
    int cardCategoryId = int(temp_cardCategoryId.getContent());
    cards[i].cardCategoryId = cardCategoryId;

    // get text
    XML temp_text = allCards[i].getChild("text");
    String text = temp_text.getContent();
    cards[i].text = text;

    // get link-content

    if (type == "link") {

      XML temp_title = allCards[i].getChild("title");
      String originalTitle = temp_title.getContent();
      String title[] = split(originalTitle, "/");

      cards[i].originalTitle = originalTitle;
      cards[i].title = title;

      cards[i].filename = removeSpecChars(title[0].toLowerCase());
      if (temp_title.hasAttribute("forcedFilename")) {
        cards[i].filename = temp_title.getString("forcedFilename");
      }

      cards[i].tags = new int[cardCategories.length];
      for (int ii = 0; ii < cardCategories.length; ii++) {
        cards[i].tags[ii] = ii;
      }

      XML allLinks[] = allCards[i].getChildren("link");
      cards[i].links = new String[allLinks.length];
      for (int ii = 0; ii < allLinks.length; ii++) {
        cards[i].links[ii] = allLinks[ii].getContent();
      }
    }

    // get display-content

    if (type != "link") {

      cards[i].originalTitle = "originalTitle";
      String title[] = split("tit/le", "/");
      cards[i].title = title;
      cards[i].filename = "filename";
      cards[i].links = new String[3];
      for (int ii = 0; ii < 3; ii++) {
        cards[i].links[ii] = "link";
      }

      XML allTags[] = allCards[i].getChildren("tag");
      cards[i].tags = new int[allTags.length];
      for (int ii = 0; ii < allTags.length; ii++) {
        cards[i].tags[ii] = allTags[ii].getIntContent();
      }
    }
  }
}

// ========================================
// load CardCategories.xml
// ========================================

void loadCardCategories() {

  XML[] allCardCategories = cardCategoriesXml.getChildren("cardCategory");

  cardCategories = new CardCategory[allCardCategories.length];

  for (int i = 0; i < allCardCategories.length; i++) {

    // get Attribute "row"
    int row = 0; // default
    if (allCardCategories[i].hasAttribute("row")) {
      int temp_row = allCardCategories[i].getInt("row");
      row = temp_row;
    }

    // get Attribute "color"
    String c = "000000"; // default
    if (allCardCategories[i].hasAttribute("color")) {
      String temp_c = allCardCategories[i].getString("color");
      c = temp_c;
    }

    // get Attribute "onlyBorder"
    boolean onlyBorder = false; // default
    if (allCardCategories[i].hasAttribute("onlyBorder")) {
      String temp_onlyBorder = allCardCategories[i].getString("onlyBorder");
      if (temp_onlyBorder.equals("true")) {
        onlyBorder = true;
      }
    }

    String[] allChildren = allCardCategories[i].listChildren();

    String title = "this is a title"; // default
    for (int ii = 0; ii < allChildren.length; ii++) {
      XML temp_title = allCardCategories[i].getChild("title");
      title = temp_title.getContent();
    }
    
    cardCategories[i] = new CardCategory(title, c, row, onlyBorder);
  }
}