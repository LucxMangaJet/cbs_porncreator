# Dissecting Games

Cards to help you analyze a game from different perspectives.

[`dissectinggames.csongorb.com`](http://dissectinggames.csongorb.com)

## Version-History

### Todos

- ?

### Release 1.0 (20. March 2018)

- added cards
	- added question cards
	- added task cards

## Credits

- **Adrian Ace Köhlmoos**
	- [ace2win.itch.io](https://ace2win.itch.io)
- **Csongor Baranyai**  
	- csongorb (at) gmail (dot) com  
	- [www.csongorb.com](http://www.csongorb.com)
- additional input by
	- **Sebastian Stamm**
		- [www.the-stamm.com/](http://www.the-stamm.com/)
	- **Florian Berger**
		- [florian-berger.de/](http://florian-berger.de/)
